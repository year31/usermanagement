/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pla
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    // Mockup
    static {
        userList .add(new User("admin","password"));
        
    }
    
    public static boolean addUser(User user){
        if(user.getUserName().equals("admin")){
            return false;
        }
        userList.add(user);
        return true;
    }
    
    public static boolean addUser(String userName, String password){
        userList.add(new User(userName, password));
        return true;
    }
    
    public static boolean updateUser(int index , User user){
        userList.set(index, user);
        return true;
    }
    
    public static User getUser(int index){
        if(index > userList.size()-1){
            return null;
        }
        return userList.get(index);
    }
    public static ArrayList<User> getUsers(){
        return userList;
    }
    
    public static ArrayList<User> searchUserName(String searchText){
        ArrayList<User> list = new ArrayList<>();
        for(User user: userList){
            if(user.getUserName().startsWith(searchText)){
                list.add(user);
            }
        }
        return list;
    }
    //del
    public static boolean delUser(int index){
        userList.remove(index);
        return true;
    } 
    public static boolean delUser(User user){
        userList.remove(user);
        return true;
    }
    
    public static User Login(String userName , String password){
        for(User user: userList){
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }
    
    public static void save(){
        FileOutputStream fos = null;
        try {
            File file = new File("SavePS2.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void load(){
        FileInputStream fis = null;
        try {
            File file = new File("SavePS2.dat");
            fis = new  FileInputStream(file);
            ObjectInputStream  ois = new ObjectInputStream(fis);
            userList=(ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
